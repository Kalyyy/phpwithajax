function getMethod(){
    // create an object
    var objectToCall = new XMLHttpRequest();
    // open a request to the backend to make connection
    objectToCall.open('GET', 'php/GetExample.php',true);
    // send the request about what content i need
    objectToCall.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    // add a function to insert the information on my backend php file and i add it to html 
    // without recharge the page
    objectToCall.onreadystatechange = function(){
        document.getElementById('getMethod').innerHTML=objectToCall.responseText;
    }
    // print the function on the element
    objectToCall.send();

}

function postMethod(){
    var objectToCall = new XMLHttpRequest();
    objectToCall.open('POST','php/PostExample.php',true);
    objectToCall.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    objectToCall.onreadystatechange = function(){
        document.getElementById('postMethod').innerHTML=objectToCall.responseText;
    }
    objectToCall.send('username=Jair');
}